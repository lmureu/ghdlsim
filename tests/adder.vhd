library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

entity adder is
    port (
        A: in std_logic_vector(7 downto 0);
        B: in std_logic_vector(7 downto 0);
        Ci: in std_logic;
        Co: out std_logic;
        Z: out std_logic_vector(7 downto 0);
        En: in std_logic;
        clk: in std_logic
    );
end entity adder;

architecture rtl of adder is
    signal Au, Bu, Ciu: unsigned(8 downto 0);
    signal output_register: unsigned(8 downto 0);
begin
    
    Au <= unsigned('0' & A);
    Bu <= unsigned('0' & B);
    Ciu <= unsigned'(X"00" & Ci);
    output_register <= Au + Bu + Ciu;

    Regs: process(clk, A, B, Ci)
    begin
        if rising_edge(clk) then
            if En = '1' then
                Co <= output_register(8);
                Z  <= std_logic_vector(output_register(7 downto 0));
            else
                Co <= 'Z';
                Z  <= "ZZZZZZZZ";
            end if;
        end if;
    end process Regs;
end architecture rtl;