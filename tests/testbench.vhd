library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

entity testbench is
end entity testbench;

architecture bench of testbench is
    component adder
        port (
            A: in std_logic_vector(7 downto 0);
            B: in std_logic_vector(7 downto 0);
            Ci: in std_logic;
            Co: out std_logic;
            Z: out std_logic_vector(7 downto 0);
            En: in std_logic;
            clk: in std_logic
        );
    end component;

    constant clock_period: time := 1 ns;
    signal simulation_run: boolean := true;

    signal A, B, Z: std_logic_vector(7 downto 0);
    signal Ci, Co, En, Clock: std_logic;
    signal TestCase: integer := 0;
begin
    uut: Adder port map (
        A => A,
        B => B,
        Ci => Ci,     
        Co => Co,
        En => En,
        Z => Z,
        Clk => Clock
    );

    clock_gen: process
    begin
        while simulation_run loop
            clock <= '0', '1' after clock_period / 2;
            wait for clock_period;
        end loop;
        wait;
    end process clock_gen;
    
    main: process
    begin
        wait until clock = '0';

        A <= X"3A";
        B <= X"0f";
        Ci <=  '0';
        En <= '0';
        wait until clock = '1';
        wait until clock = '0';
        assert Z = "ZZZZZZZZ" report "Error testcase " & integer'image(TestCase) & ".1";
        assert Co = 'Z' report "Error testcase " & integer'image(TestCase) & ".2";
        TestCase <= TestCase + 1;

        A <= X"3A";
        B <= X"0f";
        Ci <=  '0';
        En <= '1';
        wait until clock = '1';
        wait until clock = '0';
        assert Z = X"49" report "Error testcase " & integer'image(TestCase) & ".1";
        assert Co = '0' report "Error testcase " & integer'image(TestCase) & ".2";
        TestCase <= TestCase + 1;

        A <= X"3A";
        B <= X"0f";
        Ci <=  '1';
        En <= '1';
        wait until clock = '1';
        wait until clock = '0';
        assert Z = X"4a" report "Error testcase " & integer'image(TestCase) & ".1";
        assert Co = '0' report "Error testcase " & integer'image(TestCase) & ".2";
        TestCase <= TestCase + 1;

        A <= X"ff";
        B <= X"00";
        Ci <=  '1';
        En <= '1';
        wait until clock = '1';
        wait until clock = '0';
        assert Z = X"00" report "Error testcase " & integer'image(TestCase) & ".1";
        assert Co = '1' report "Error testcase " & integer'image(TestCase) & ".2";
        TestCase <= TestCase + 1;

        A <= X"44";
        B <= X"22";
        Ci <=  '1';
        En <= '1';
        wait until clock = '1';
        wait until clock = '0';
        report "The following test cases should fail: " & integer'image(TestCase) & ".1 and " & integer'image(TestCase) & ".2";
        assert Z = X"00" report "Error testcase " & integer'image(TestCase) & ".1 (expected failure)" severity note;
        assert Co = '1' report "Error testcase " & integer'image(TestCase) & ".2 (expected failure)" severity note;
        TestCase <= TestCase + 1;
        
        -- Stop the simulation
        simulation_run <= false;
    end process main;
end architecture bench;