library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

entity counter is
    port (
        clk: in std_logic;
        input: in std_logic_vector(7 downto 0);
        output: out std_logic_vector(7 downto 0);
        reset, enable, set, output_enable: in std_logic 
    );
end entity counter;

architecture rtl of counter is
    signal counter_r: unsigned(7 downto 0);
begin
    registers: process(clk)
    begin
        if rising_edge(clk) then
            if reset = '1' then
                counter_r <= X"00";
            elsif enable = '1' then
                if set = '1' then
                    counter_r <= unsigned(input);
                else
                    counter_r <= counter_r + 1;
                end if;
            end if;
        end if;
    end process registers;
    
    output <= std_logic_vector(counter_r) when output_enable = '1' else "ZZZZZZZZ";
end architecture rtl;
