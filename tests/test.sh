alias ghdlsim="~/.local/bin/ghdlsim"

cat > /dev/stderr <<EOF 
============================================================================

This is the ghdlsim test suite.
You should expect the last two test fail, but the simulation shouldn't stop.

============================================================================
EOF

ghdlsim