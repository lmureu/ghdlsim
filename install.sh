#!/usr/bin/env zsh

INSTALL_DIR=$HOME/.local/bin

if (($path[(Ie)$INSTALL_DIR])) then
    set -x
    ln -fs "$(readlink -f ghdlsim)" "$INSTALL_DIR"
else
    echo Error: $INSTALL_DIR is not in \$PATH
    exit 1
fi